**Desarrollo de aplicaciones con conexion a base de datos**
**Castro Valenzuela Paula Alejandra**
**5AVP**


-Práctica #1 - 02/09/2022 - practica1 para probar como se suben los archivos
commit: e09147b12f066c00bf0bddf687d47bc2738d4e02
archivo: https://gitlab.com/AlejandraCastro/desarrolla_aplicaciones_web/-/blob/main/parcial1/practica_ejemplo.html 

-Práctica #2 - 09/09/2022 - Practica javaScript
commit: 490cb3cd7c50abcdc557beeecaaa6fa419f74318

-Práctica #3 - 15/09/2022 - Práctica Web con Conexión a Base de Datos
commit: 37fb88ee7478ee0ba302e9489061c0afc87d5f80

-Práctica #4 - 19/09/2022 - Vista de registro de datos
commit: 55b772698e804dd37e11cf50536c1e28c1c67caf

-Práctica #5 - 22/09/22 - Registro de base de datos
commit: cafae1415c41e67354c7577f81152c9963497b23

-Práctica #6 - 26/09/22 - Consultar Datos
commit: 270ad5e09a0ac12fadb06a4b8d9bfe8f820064d3

-Práctica #7 - 26/09/22 - Conexión
commit: efabaafb1f632a54ca91cccce58a124ff3892a58
